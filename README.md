# Cours de html

## Description

Ce repo contient le code écrit durant le cours de html à Pop School Lens avec la promo 2 (2017).

## Installation

Downloadez un zip du repo ou clonez-le avec git.

## Structure type d'un projet html et css statique

    document-root/
      css/
        *.css
      img/
        *.gif
        *.jpg
        *.jpeg
        *.png
        *.svg
      vendor/
        */
        bootstrap/
        jquery/
      *.html
      index.html

## Démarrage d'un serveur web de développement

### Récupération de l'adresse ip du poste de développement

Ouvrir un terminal puis taper :

    sudo ifconfig

La commande renvoit des infos du type :

    eth0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
            ether 00:00:00:00:00:00  txqueuelen 1000  (Ethernet)
            RX packets 0  bytes 0 (0.0 B)
            RX errors 0  dropped 0  overruns 0  frame 0
            TX packets 0  bytes 0 (0.0 B)
            TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
            device interrupt 20  memory 0xf2600000-f2620000

    lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
            inet 127.0.0.1  netmask 255.0.0.0
            inet6 ::1  prefixlen 128  scopeid 0x10<host>
            loop  txqueuelen 1  (Local Loopback)
            RX packets 12  bytes 720 (720.0 B)
            RX errors 0  dropped 0  overruns 0  frame 0
            TX packets 12  bytes 720 (720.0 B)
            TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

    wlan0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
            inet 192.168.0.36  netmask 255.255.255.0  broadcast 192.168.0.255
            inet6 0000::0000:0000:0000:0000  prefixlen 64  scopeid 0x20<link>
            ether 00:00:00:00:00:00  txqueuelen 1000  (Ethernet)
            RX packets 3309  bytes 3096484 (2.9 MiB)
            RX errors 0  dropped 0  overruns 0  frame 0
            TX packets 2871  bytes 464013 (453.1 KiB)
            TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

Le champ `inet` de l'interface `wlan0` indique l'adresse ip recherchée (ici `192.168.0.36`).

### Pages accessibles depuis l'extérieur

Pour démarrer un serveur web accessible depuis l'extérieur (téléphone, tablette), ouvrir un terminal puis taper :

    cd nom-du-dossier
    php -S 0.0.0.0:8000

Puis dans la barre d'adresse du navigateur (de votre poste, mobile ou tablette), taper (en prenant soin de remplacer `192.168.0.36` par l'adresse ip de votre poste) :

    http://192.168.0.36:8000/nom-du-fichier.html

### Pages accessibles depuis le poste de développement uniquement

Pour démarrer un serveur web accessible uniquement depuis le poste de développement, ouvrir un terminal puis taper :

    cd nom-du-dossier
    php -S 127.0.0.1:8000

Puis dans la barre d'adresse du navigateur (de votre poste), taper :

    http://127.0.0.1:8000/nom-du-fichier.html

## Images

- file: `SplitShire-01650.jpg`
  url: [Boat Bow](https://www.splitshire.com/boat-bow/)
  licence: [License - SplitShire - Free Stock Photos & Images](https://www.splitshire.com/licence/)
- file: `iconmonstr-code-2-32.png`
  url: [Code 2 - PNG - iconmonstr](https://iconmonstr.com/code-2/?png)
  licence: [License Agreement - iconmonstr](https://iconmonstr.com/license/)

## Typo

- files: `opensans_*_macroman/*`
  url: [Open Sans Font Free by Ascender Fonts » Font Squirrel](https://www.fontsquirrel.com/fonts/open-sans)
  licence: [Licenses](http://www.apache.org/licenses/)

